/*
 *  Copyright (c) 2019-2020, 冷冷 (wangiegie@gmail.com).
 *  <p>
 *  Licensed under the GNU Lesser General Public License 3.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  <p>
 * https://www.gnu.org/licenses/lgpl.html
 *  <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pig4cloud.pig.common.core.constant;

/**
 * @author lengleng
 * @date 2019-10-28
 * <p>
 * 缓存的key 常量
 */
public interface CacheConstants {

	/**
	 * 菜单信息缓存 CacheConstants.MENU_DETAILS
	 */
	String MENU_DETAILS = "pig_plus_menu_details";

	/**
	 * 用户信息缓存 CacheConstants.USER_DETAILS
	 */
	String USER_DETAILS = "pig_plus_user_details";

	/**
	 * 字典信息缓存 CacheConstants.DICT_DETAILS
	 */
	String DICT_DETAILS = "pig_plus_dict_details";

}
