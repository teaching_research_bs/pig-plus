/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pig.admin.controller;

import java.util.Map;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.admin.entity.SysConfig;
import com.pig4cloud.pig.admin.service.SysConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 系统配置表
 *
 * @author Lucky
 * @date 2019-11-23 17:32:44
 */
@RestController
@AllArgsConstructor
@RequestMapping("/config")
public class ConfigController {

  private final SysConfigService configService;

  /**
   * 分页查询
   * @param page 分页对象
   * @param config 系统配置表
   * @return
   */
  @GetMapping("/page" )
  public R getConfigPage(Page page, SysConfig config) {
    return R.ok(configService.page(page, Wrappers.query(config)));
  }


  /**
   * 通过id查询系统配置表
   * @param id id
   * @return R
   */
  @GetMapping("/{id}" )
  public R getById(@PathVariable("id" ) Integer id) {
    return R.ok(configService.getById(id));
  }

  /**
   * 新增系统配置表
   * @param config 系统配置表
   * @return R
   */
  @SysLog("新增系统配置表" )
  @PostMapping
  @PreAuthorize("@pms.hasPermission('admin_config_add')" )
  public R save(@RequestBody SysConfig config) {
    return R.ok(configService.save(config));
  }

  /**
   * 修改系统配置表
   * @param config 系统配置表
   * @return R
   */
  @SysLog("修改系统配置表" )
  @PutMapping
  @PreAuthorize("@pms.hasPermission('admin_config_edit')" )
  public R updateById(@RequestBody SysConfig config) {
    return R.ok(configService.updateById(config));
  }

  /**
   * 通过id删除系统配置表
   * @param id id
   * @return R
   */
  @SysLog("通过id删除系统配置表" )
  @DeleteMapping("/{id}" )
  @PreAuthorize("@pms.hasPermission('admin_config_del')" )
  public R removeById(@PathVariable Integer id) {
    return R.ok(configService.removeById(id));
  }

	/**
	 * 修改系统配置表
	 * @param config 系统配置表
	 * @return R
	 */
	@SysLog("修改系统配置表" )
	@PostMapping("/wx")
	//@PreAuthorize("@pms.hasPermission('admin_config_edit')" )
	public R updateWx(@RequestBody SysConfig config) {
		return R.ok(configService.updateById(config));
	}

}
